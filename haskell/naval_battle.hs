import Random
import Array
import System.IO
import Graphics.UI.Gtk
type Board = Array Int (Array Int Int)

board_size = (20, 20)
board_empty = 1
--board_itens = [(4, 2, board_empty + 1), (3, 3, board_empty + 2), (1, 5, board_empty + 3), (5, 1, board_empty + 4)]
board_itens = [(1, 3, board_empty + 1)]
board_itens_total = sum (map (\(x, y, _) -> x * y) board_itens)

create_board :: Int -> Int -> Int -> Board
create_board m n v = listArray (1, m) [listArray (1, n) [v | i <- [1..n]] | i <- [1..m]]
main :: IO ()
main = do
	initGUI
	window <- windowNew
	button <- buttonNew
	set window [ containerBorderWidth := 10, containerChild := button ]
	set button [ buttonLabel := "Hello World" ]
	onClicked button (putStrLn "Hello World")
	onDestroy window mainQuit
	widgetShowAll window
	mainGUI

amain = do 
	g <- getStdGen
	hSetBuffering stdout NoBuffering
	game_loop (populate_board (create_board (fst board_size) (snd board_size) board_empty) board_itens g) (create_board (fst board_size) (snd board_size) 0) 0

board_to_string board = " " ++ unwords [s ++ "\n" | s <- [unwords [(show $ board!i!j) ++ " "  | j <- range $ bounds(board!i)] | i <- range (bounds(board))]]

game_loop :: Board -> Board -> Int -> IO()
game_loop board user_board itens_found = do
	print "GameBoard:"
	putStr $ board_to_string board
	print "UserBoard:"
	putStr $ board_to_string user_board
	print ("Found: " ++ (show itens_found)  ++ "/ " ++ (show board_itens_total))
	if itens_found == board_itens_total 
		then do
			print "Cogratulations!"
		else do
			print "x:"
			x <- readLn
			if x == 0
				then do
					print "Good Bye!"
				else do 
					print "y:"
					y <- readLn
					game_loop board (board_set x y user_board (board!x!y)) (itens_found + (bool_to_int (board!x!y /= board_empty && user_board!x!y == 0)))

board_set :: Int -> Int -> Board -> Int -> Board
board_set x y board v = board // [(x, board!x // [(y, v)])]

populate_board board [] _ = board
populate_board board (x : rest) g = 
	let (nboard, ng) = populate_board_item board x g
	in populate_board nboard rest ng

populate_board_item board (0, _, _) g  = (board, g)
populate_board_item board (count, size, id) g = 
	let (nboard, ng) = populate_board_item_size board size id g
	    (rboard, rg) = populate_board_item nboard ((count - 1), size, id) ng
	in (rboard, rg)

populate_board_item_size board size id g = 
	let (direction, ng) = randomR(0, 1) g
	    (rboard, rg) = populate_board_item_size_direction board size direction id ng
	in (rboard, rg)

populate_board_item_size_direction board size direction id g 
		| valid_fill board i j direction size = (fill_board board i j direction size id, g2)
		| True                          = (populate_board_item_size_direction board size direction id g2)
		where 
			(i, g1) = randomR(1, (fst board_size) - direction * size) g
			(j, g2) = randomR(1, (snd board_size) - (1 - direction) * size) g1

fill_board :: Board -> Int -> Int -> Int -> Int -> Int -> Board
fill_board board i j direction 0 id = board
fill_board board i j direction size id = fill_board (board_set i j board (id)) (i + direction) (j + 1 - direction) direction (size - 1) id

valid_fill :: Board -> Int -> Int -> Int -> Int -> Bool
valid_fill board i j direction 0 = True
valid_fill board i j direction size = (board!i!j) == board_empty && (valid_fill board (i + direction) (j + 1 - direction) direction (size - 1))

bool_to_int :: Bool -> Int
bool_to_int b
	| b    = 1
	| True = 0

